#include <regex>

#include "chord.path.h"
#include "chord.file.h"

using namespace std;
namespace fs = std::experimental::filesystem;

namespace chord {

path::path(const fs::path &other)
    : _path{other} {}

path path::filename() const { return path{_path.filename()}; }

path path::extension() const { return path{_path.extension()}; }

path path::parent_path() const { return path{_path.parent_path()}; }

std::set<path> path::recursive_contents() const {
  std::set<path> files_and_dirs;
  for (const auto& entity : std::experimental::filesystem::recursive_directory_iterator(_path)) {
    files_and_dirs.emplace(entity);
  }
  return files_and_dirs;
}

std::set<path> path::contents() const {
  std::set<path> files_and_dirs;
  if (chord::file::is_regular_file(_path)) {
    return files_and_dirs;
  }

  for (const auto& entity : std::experimental::filesystem::directory_iterator(_path)) {
    files_and_dirs.emplace(entity);
  }
	return files_and_dirs;
}

std::string path::string() const { return _path.string(); }

set<path> path::all_paths() const {
  set<path> paths;
  path current_path;
  for (const auto &dir : canonical()._path) {
    current_path /= dir;
    paths.emplace(current_path);
  }
  return paths;
}

path path::canonical() const {
  path result;
  for (const auto &dir : _path) {
    if (dir=="..") {
      result = result.parent_path();
    } else if (dir==".") {
      //ignore
    } else if (dir=="/" && result.string().back() == '/') {
      //ignore
    } else {
      auto str = dir.string();
      auto pos = str.rfind(fs::path::preferred_separator);
      // note: because std::path allows //<part>, we have to remove preceding separators
      result /= (pos != string::npos) ? path{str.substr(pos)} : dir;
    }
  }
  return result;
}

bool path::empty() const noexcept {
  return _path.empty();
}

path::operator std::string() const { return _path.string(); }

/**
 * example: {/tmp/foo/file1.txt}-{/tmp/foo} => file1.txt
 */
path path::operator-(const path &p) const {
  path result;
  auto can_rop = p.canonical();
  auto can_lop = canonical();

  regex pattern(can_rop.string());
  return path{regex_replace(can_lop.string(), pattern, "")}.canonical();
  //auto j = begin(can_rop._path);
  //for (auto i = begin(can_lop._path); i != end(can_lop._path); i++) {
  //  if (j != end(can_rop._path) && *i == *j){
  //    ++j;
  //    continue;
  //  } 
  //  result /= *i;
  //}
  //return result.canonical();
}

path path::operator/=(const path &p) { return path{_path /= p._path}; }

path path::operator/(const path &p) const { return path{_path / p._path}; }

bool path::operator==(const path &p) const { return _path==p._path; }

bool path::operator<(const path &p) const {
  return _path < p._path;
}

bool path::operator>(const path &p) const {
  return _path > p._path;
}

bool path::operator==(const std::string &s) const { return s==string(); }

bool operator==(const std::string &p1, const path &p2) {
  return p1==p2.string();
}

std::ostream &operator<<(std::ostream &os, const path &path) {
  return os << path.string();
}

} //namespace chord
