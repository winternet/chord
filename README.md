```
                                                      
     _/_/_/  _/                                  _/   
  _/        _/_/_/      _/_/    _/  _/_/    _/_/_/    
 _/        _/    _/  _/    _/  _/_/      _/    _/     
_/        _/    _/  _/    _/  _/        _/    _/      
 _/_/_/  _/    _/    _/_/    _/          _/_/_/       
                                                      
                                                      
```

[![Build Status](https://circleci.com/gh/winternet/chord/tree/master.svg?style=shield&circle-token=06884550effac32786aa01b3638bdd15e8baa03b)](https://circleci.com/gh/winternet/chord) [![codecov](https://codecov.io/gh/winternet/chord/branch/master/graph/badge.svg)](https://codecov.io/gh/winternet/chord)

## Installation

See [INSTALL.md](INSTALL.md) for installation instructions.
